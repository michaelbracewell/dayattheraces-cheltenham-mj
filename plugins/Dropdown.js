import Vue from 'vue';
import Dropdown from 'hsy-vue-dropdown';

const warn = true;
const message = 'NOTICE: VueDropdown plugin initialised (this can be removed if not using the Box Grid Template/Using a Box grid without sorting enabled)';
if (warn) console.warn(message);
Vue.use(Dropdown);
