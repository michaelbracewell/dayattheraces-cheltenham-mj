import Vue from 'vue';
import HighchartsVue from 'highcharts-vue';

Vue.use(HighchartsVue);

const warn = true;
const message = 'NOTICE: Highcharts plugin initialised (this increases payload size, please disable in nuxt.config if it\'s not needed)';
if (warn) console.warn(message);
