import Vue from 'vue';
import Isotope from 'vueisotope';
import imagesLoaded from 'vue-images-loaded';

const IsotopeComp = {
  install(Vue) {
    Vue.component('IsotopeComp', Isotope);
    Vue.directive('imagesLoaded', imagesLoaded);
  },

};

const warn = true;
const message = 'NOTICE: VueIsotope plugin initialised (this can be removed if not using the Box Grid Template)';
if (warn) console.warn(message);
Vue.use(IsotopeComp);
export default Isotope;
