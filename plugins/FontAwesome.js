import Vue from 'vue';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

const FontAwesome = {
  install(Vue) {
    Vue.component('FontAwesomeIcon', FontAwesomeIcon);
  },

};

Vue.use(FontAwesome);
export default FontAwesomeIcon;
