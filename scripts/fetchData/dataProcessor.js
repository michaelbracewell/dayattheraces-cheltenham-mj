// const camelCase = require('camelcase');
const GoogleSpreadsheet = require('google-spreadsheet');
const async = require('async');
const each = require('async-each-series');
const fs = require('fs');
const _ = require('lodash');
// const path = require('path');

const worksheetData = {};

// filter returned columns with this list. ref in rowToJson()
// const whiteListCols = [
//   // 'task',
//   'description',
//   'assigned',
//   'complete',
// ];

// remove these columns from list. ref in rowToJson()
const blackListCols = [
  'id',
  'app:edited',
  '_xml',
  '_links',
  '_chk2m',
  'save',
  'del',
];

function fetchData(spreadsheetId, step) {
  const doc = new GoogleSpreadsheet(spreadsheetId);

  async.series([
    (stepInner) => { // setAuth
      const credsPath = `${__dirname}/google-generated-creds.json`;
      const creds = JSON.parse(fs.readFileSync(credsPath, 'utf8'));
      doc.useServiceAccountAuth(creds, stepInner);
    },
    (stepInner) => { // getInfoAndWorksheets
      doc.getInfo((err, info) => {
        each(info.worksheets, (worksheet, next) => {
          worksheetData[worksheet.title] = [];
          worksheet.getRows({
            offset: 0,
            limit: 10000,
          }, (err2, rows) => {
            worksheetData[worksheet.title] = rows;
            next();
          });
        }, () => {
          step();
          stepInner();
        });
      });
    },
  ], (err3) => {
    if (err3) {
      console.log(`Error: ${err3}`);
    }
  });
}

function rowToJson(row) {
  const rowData = {};

  Object.keys(row).forEach((key) => {
    const rowHeading = key;
    const notBlackList = (blackListCols.indexOf(rowHeading) === -1);
    // const inWhiteList = (whiteListCols.indexOf(rowHeading) !== -1);
    if (notBlackList) { // change var to notBlackList/inWhiteList to control what data is exported
      const headingString = String(rowHeading);
      rowData[headingString] = row[rowHeading];
    }
  });
  return rowData;
}


// function isEmpty(val) {
//   return (val === undefined || val == null || val.length <= 0);
// }

function processStats(dataOrdered) {
  const rtn = {};

  Object.keys(dataOrdered).forEach((sheetKey) => {
    if (sheetKey === 'copy') {
      rtn.copy = {};
      const copies = _.groupBy(dataOrdered.copy, 'name');
      Object.keys(copies).forEach((keyCopy) => {
        rtn.copy[keyCopy] = copies[keyCopy][0].copy;
      });
    } else {
      rtn[sheetKey] = dataOrdered[sheetKey];
    }
  });

  return rtn;
}

module.exports = {
  acquireData(spreadsheetId, step) {
    fetchData(spreadsheetId, step);
  },
  arrangeData() {
    const data = worksheetData;
    const dataOrdered = {};

    Object.keys(data).forEach((key) => {
      const worksheet = data[key];
      const spreadSheetJson = [];
      Object.keys(worksheet).forEach((key2) => {
        const row = worksheet[key2];
        const rowJson = rowToJson(row);
        spreadSheetJson.push(rowJson);
      });
      if (key.charAt(0) !== '_') dataOrdered[key] = spreadSheetJson;
    });

    dataOrdered.outData = processStats(dataOrdered);

    return dataOrdered.outData;
  },
};
