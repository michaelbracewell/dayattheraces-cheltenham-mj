const fs = require('fs');
const async = require('async');
const dataProcessor = require('./dataProcessor');
const envConfig = require('../../env-config.js');

let data = {};

async.series([
  (step) => { // get data
    dataProcessor.acquireData(envConfig.contentGoogleSheet, step);
  },
  () => { // process data
    data = dataProcessor.arrangeData();
    fs.writeFileSync(`${process.cwd()}/assets/data/real.json`, JSON.stringify(data));
  },
], (err) => {
  if (err) {
    console.log(`Error: ${err}`);
  }
});
