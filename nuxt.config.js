const path = require('path');
const urlValidate = require('valid-url');
const urlJoin = require('url-join');
const appConfig = require('./appConfig.js').default;

// const appData = require('./assets/data/dataIndex.js');

/* ----- functions ----- */
function shouldPrintCanonical() {
  if (appConfig.canonical_url) {
    if (urlValidate.isUri(appConfig.canonical_url)) return true;
  }
  return false;
}

function trimSlashes(string) {
  let copy = string;
  copy = copy.trim();
  if (copy.charAt(0) === '/') copy = copy.substring(1); // remove any leading slash
  copy = copy.split('').reverse().join(''); // reverse
  if (copy.charAt(0) === '/') copy = copy.substring(1); // remove any trailing (now leading) slash
  copy = copy.split('').reverse().join(''); // reverse back
  return copy;
}

function getfullBaseUrl() {
  const env = process.env.NODE_ENV;
  if (!env || env === 'development') return '';
  const fullBaseUrl = trimSlashes(appConfig[env].url)
    .toLowerCase();

  return fullBaseUrl;
}
const fullBaseUrl = getfullBaseUrl();

function getSubfolder() {
  const env = process.env.NODE_ENV;
  if (!env || env === 'development') return '';
  if (!appConfig[env].subfolder) return '';
  return trimSlashes(appConfig[env].subfolder)
    .toLowerCase();
}
const subfolder = getSubfolder();

function isVanity() {
  return Boolean(process.env.VANITY);
}
const vanity = isVanity();

function getEnvironment() {
  return process.env.NODE_ENV || 'local';
}
const environment = getEnvironment();
/* ----- end functions ----- */


// const hrefLangTags = appData.links.map((link) => {
//   let url;

//   if (environment === 'staging') url = link.stagingurl;
//   else if (environment === 'vanity') url = link.vanityurl;
//   else url = link.productionurl;

//   return {
//     rel: 'alternate',
//     href: url,
//     hreflang: link.language,
//   };
// });

const config = {
  env: {
    fullBaseUrl,
    subfolder,
    environment,
    vanity,
  },

  /* eslint-disable object-curly-newline */
  head: {
    htmlAttrs: {
      lang: 'en',
    },
    titleTemplate: appConfig.titleTemplate,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: appConfig.defaultDescription },
      { hid: 'og:type', name: 'og:type', content: 'website' },
      { hid: 'og:title', name: 'og:title', content: vanity ? appConfig.vanitySiteName : appConfig.siteName },
      { hid: 'og:url', name: 'og:url', content: '___' }, // this will be overwritten for staging/production deploys
      { hid: 'og:description', name: 'og:description', content: appConfig.facebook.description },
      { hid: 'og:image', name: 'og:image', content: urlJoin(fullBaseUrl, subfolder, 'social.jpg') },
      { hid: 'twitter:card', name: 'twitter:card', content: 'summary' },
      { hid: 'twitter:site', name: 'twitter:site', content: vanity ? '' : appConfig.twitter.site },
      { hid: 'twitter:title', name: 'twitter:title', content: appConfig.twitter.title },
      { hid: 'twitter:description', name: 'twitter:description', content: appConfig.twitter.description },
      { hid: 'twitter:creator', name: 'twitter:creator', content: vanity ? '' : appConfig.twitter.creator },
      { hid: 'twitter:image', name: 'twitter:image', content: urlJoin(fullBaseUrl, subfolder, 'social.jpg') },
      { hid: 'application-name', name: 'application-name', content: vanity ? appConfig.vanitySiteName : appConfig.siteName },
      { hid: 'msapplication-TileColor', name: 'msapplication-TileColor', content: '#ffffff' },
      { hid: 'msapplication-TileImage', name: 'msapplication-TileImage', content: path.join('/', subfolder, 'favicons/mstile-144x144.png') },
      { hid: 'msapplication-square70x70logo', name: 'msapplication-square70x70logo', content: path.join('/', subfolder, 'favicons/mstile-70x70.png') },
      { hid: 'msapplication-square150x150logo', name: 'msapplication-square150x150logo', content: path.join('/', subfolder, 'favicons/mstile-150x150.png') },
      { hid: 'msapplication-wide310x150logo', name: 'msapplication-wide310x150logo', content: path.join('/', subfolder, 'favicons/mstile-310x150.png') },
      { hid: 'msapplication-square310x310logo', name: 'msapplication-square310x310logo', content: path.join('/', subfolder, 'favicons/mstile-310x310.png') },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: path.join('/', subfolder, 'favicons/favicon.ico') },
      { rel: 'apple-touch-icon-precomposed', sizes: '57x57', href: path.join('/', subfolder, 'favicons/apple-touch-icon-57x57.png') },
      { rel: 'apple-touch-icon-precomposed', sizes: '114x114', href: path.join('/', subfolder, 'favicons/apple-touch-icon-114x114.png') },
      { rel: 'apple-touch-icon-precomposed', sizes: '72x72', href: path.join('/', subfolder, 'favicons/apple-touch-icon-72x72.png') },
      { rel: 'apple-touch-icon-precomposed', sizes: '144x144', href: path.join('/', subfolder, 'favicons/apple-touch-icon-144x144.png') },
      { rel: 'apple-touch-icon-precomposed', sizes: '60x60', href: path.join('/', subfolder, 'favicons/apple-touch-icon-60x60.png') },
      { rel: 'apple-touch-icon-precomposed', sizes: '120x120', href: path.join('/', subfolder, 'favicons/apple-touch-icon-120x120.png') },
      { rel: 'apple-touch-icon-precomposed', sizes: '76x76', href: path.join('/', subfolder, 'favicons/apple-touch-icon-76x76.png') },
      { rel: 'apple-touch-icon-precomposed', sizes: '152x152', href: path.join('/', subfolder, 'favicons/apple-touch-icon-152x152.png') },
      { rel: 'icon', sizes: '196x196', type: 'image/png', href: path.join('/', subfolder, 'favicons/favicon-196x196.png') },
      { rel: 'icon', sizes: '96x96', type: 'image/png', href: path.join('/', subfolder, 'favicons/favicon-96x96.png') },
      { rel: 'icon', sizes: '32x32', type: 'image/png', href: path.join('/', subfolder, 'favicons/favicon-32x32.png') },
      { rel: 'icon', sizes: '16x16', type: 'image/png', href: path.join('/', subfolder, 'favicons/favicon-16x16.png') },
      { rel: 'icon', sizes: '128x128', type: 'image/png', href: path.join('/', subfolder, 'favicons/favicon-128.png') },
    ],
  },
  /* eslint-enable */

  minify: {
    removeOptionalTags: false,
  },

  // loading: false,
  loading: { color: '#ccc' },

  css: [
    'normalize.css',
    '~/assets/sass/globals.sass',
    '~/assets/sass/transitions.sass',
  ],

  router: {
    linkActiveClass: 'active',
    base: '/',
  },

  build: {
    // analyze: true,
    // babel: {
    //   babelrc: true,
    // },
    vendor: [
      'vue-clipboard2',
      'vue-textarea-autosize',
      'babel-polyfill',
      'eventsource-polyfill',
      'nodelist-foreach-polyfill',
      'smoothscroll',
    ],
    publicPath: '/_app/',
  },
  generate: {
    dir: `_build/${subfolder}`,
  },
  plugins: [
    '~/plugins/vueClipboard.js',
    '~/plugins/vueTextareaAutosize.js',
    {
      ssr: false,
      src: '~/plugins/waypoints.js',
    },
    // {
    //   ssr: false,
    //   src: '~/plugins/highcharts.js',
    // },
    // // Boxgrid no-ssr plugins
    // {
    //   src: '~/plugins/IsotopeComp.js',
    //   ssr: false,
    // },
    // // Only needed if using the sort option
    // {
    //   src: '~/plugins/Dropdown.js',
    //   ssr: false,
    // },
  ],
};

(function addConditionalVals() {
  if (shouldPrintCanonical()) {
    config.head.link.push({
      rel: 'canonical',
      href: appConfig.canonical_url,
    });
  }

  config.router.base = (subfolder === '') ? '/' : `/${subfolder}`;
  if (fullBaseUrl) {
    const index = config.head.meta.findIndex(item => item.hid === 'og:url');
    config.head.meta[index].content = fullBaseUrl;
  }
}());

module.exports = config;
