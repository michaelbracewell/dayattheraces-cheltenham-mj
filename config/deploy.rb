# config valid only for current version of Capistrano
lock '3.11.0'

set :application, 'markjarvis-adayattheraces-chelthenham' # <----- eg myracing-gg-gen
set :repo_url, 'git@bitbucket.org:michaelbracewell/dayattheraces-cheltenham-mj.git' # <----- eg git@bitbucket.org:blueclaw/myracing-gggenerator.git
set :branch, 'master' # <----- eg staging
set :app_type, 'nuxt' # <----- possible values are: 'gulp','next','nuxt', 'wordpress', 'rails', 'laravel'

check_values_filled
