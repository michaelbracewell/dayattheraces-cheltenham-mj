set :application_url, 'markjarvis-adayattheraces-chelthenham.staging.brewingatbc.co.uk' # <----- mytest.staging.brewingatbc.co.uk (no http or https, no slashes)
setup_config

role :app, %w{ubuntu@staging.brewingatbc.co.uk}

server 'staging.brewingatbc.co.uk',
  user: 'ubuntu',
  roles: %w{app},
  ssh_options: {
    keys: %w(~/.ssh/id_rsa),
    forward_agent: true,
    auth_methods: %w(publickey)
  }

namespace :deploy do
  run_tasks
end
