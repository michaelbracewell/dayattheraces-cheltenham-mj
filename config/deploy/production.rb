set :branch, 'master'
setup_config

role :app, %w{ubuntu@staging.brewingatbc.co.uk}

server 'staging.brewingatbc.co.uk',
  user: 'ubuntu',
  roles: %w{app},
  ssh_options: {
    keys: %w(~/.ssh/id_rsa),
    forward_agent: true,
    auth_methods: %w(publickey)
  }

namespace :deploy do
  run_tasks
end