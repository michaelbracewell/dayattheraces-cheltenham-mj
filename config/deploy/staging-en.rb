set :application_url, '___' # <----- mytest-en.staging.brewingatbc.co.uk (no http or https, no slashes)
set :application, '___' # eg. netbet-royal-families-en
set :stage, :staging
setup_config

set :default_env, {
  'TRANSLATION' => 'en'
}
role :app, %w{ubuntu@staging.brewingatbc.co.uk}

server 'staging.brewingatbc.co.uk',
  user: 'ubuntu',
  roles: %w{app},
  ssh_options: {
    keys: %w(~/.ssh/id_rsa),
    forward_agent: true,
    auth_methods: %w(publickey)
  }

namespace :deploy do
  run_tasks
end
