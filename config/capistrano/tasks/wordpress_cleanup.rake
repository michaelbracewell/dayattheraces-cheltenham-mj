desc "wordpress cleanup"
task :wordpress_cleanup do
  on roles(:app) do
    within release_path do 
      execute :rm, "-rf", "gulp", "config", "web"
      execute :find, '.', '-maxdepth', '1', '-type', 'f', '-not', '-name', '.env', '-exec', 'rm', '-r', '{} \;'

      within 'database' do
        execute :find, '.', '-maxdepth', '1', '-type', 'f', '-not', '-name', 'ca-bundle.pem', '-exec', 'rm', '-r', '{} \;'
      end

      execute :echo, "Deployed #{fetch(:application)} project on #{fetch(:application_url)}"
    end
  end
end
