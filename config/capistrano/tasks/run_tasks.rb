
def run_tasks
  app_type = fetch(:app_type)
  raise "Missing app_type" if app_type.nil?

  on roles(:app) do
    case app_type
    when "gulp"
      before :publishing, :build_gulp
      after :build_gulp, :blueclaw_cleanup

      if fetch(:stage) == :staging
        after :blueclaw_cleanup, :webserver_config_update
      end
    when "next"
      before :publishing, :build_next
      after :build_next, :blueclaw_cleanup

      if fetch(:stage) == :staging
        after :blueclaw_cleanup, :webserver_config_update
      end
    when "nuxt"
      before :publishing, :build_nuxt
      after :build_nuxt, :blueclaw_cleanup

      if fetch(:stage) == :staging
        after :blueclaw_cleanup, :webserver_config_update
      end
    when "rails"
      after :publishing, :build_rails

      if fetch(:stage) == :staging
        after :build_rails, :webserver_config_update
        after :webserver_config_update, :restart_puma
      end
    when "wordpress"
      before :publishing, :build_wordpress
      after :build_wordpress, :wordpress_cleanup

      if fetch(:stage) == :staging
        after :wordpress_cleanup, :webserver_config_update
      end
    when "laravel"
      before :publishing, :build_laravel

      if fetch(:stage) == :staging
        after :build_laravel, :webserver_config_update
      end
    else
      raise "Incorrect app_type" if app_type.nil?
    end
  end
end

def setup_config
  set :keep_releases, 2

  set :nodenv_type, :user
  set :nodenv_node, '9.11.2'
  set :nodenv_prefix, "NODENV_ROOT=#{fetch(:nodenv_path)} NODENV_VERSION=#{fetch(:nodenv_node)} #{fetch(:nodenv_path)}/bin/nodenv exec"
  set :nodenv_map_bins, %w{node npm gulp sass}
  set :nodenv_roles, :all

  set :rbenv_type, :user
  set :rbenv_ruby, '2.5.3'
  set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
  set :rbenv_map_bins, %w{rake gem bundle ruby rails}
  set :rbenv_roles, :all # default value

  app_type = fetch(:app_type)
  raise "Missing app_type" if app_type.nil?

  app_url = fetch(:application_url)

  if fetch(:stage) == :staging and app_url.nil?
    raise "Missing application_url"
  end

  unless app_url[/\Ahttp:\/\//] || app_url[/\Ahttps:\/\//]
    app_url = "http://#{app_url}"
  end

  app_url = URI.parse(app_url)
  set :application_url, app_url.host
  set :deploy_to, "/var/www/#{fetch(:application)}"

  if fetch(:stage) == :production
    set :nodenv_map_bins, %w{}
    set :nodenv_roles, []
    set :rbenv_map_bins, %w{}
    set :rbenv_roles, []
  end

  case app_type
  when "gulp"
    set :publish_dir, '_build'
  when "next"
    set :publish_dir, '_build'
  when "nuxt"
    set :publish_dir, '_build'
  when "rails"
    set :publish_dir, 'public'
    set :linked_dirs, %w{tmp log}
    set :linked_files, %w{.env}
  when "wordpress"
    set :publish_dir, 'public'
    set :linked_dirs, %w{public/wp-content/uploads public/wp-content/plugins vendor}
    set :linked_files, %w{.env}
  when "laravel"
    set :publish_dir, 'public'
    set :linked_dirs, %w{storage vendor}
    set :linked_files, %w{.env}
  else
    raise "Incorrect app_type" if app_type.nil?
  end
end

def check_values_filled
  # Front end template two specific rules
  warn ""
  warn "-----------------------------------------"
  warn "Change application name if not deploying front-end-template-2" if fetch(:application) == 'front-end-template-2'
  warn "Change repo url if not deploying front-end-template-2" if fetch(:repo_url) == 'git@bitbucket.org:blueclaw/frontendtemplate2.git'
  warn "-----------------------------------------"
  warn ""
  # End Front end template two rules
  raise "Missing application name" if fetch(:application).nil? or fetch(:application) == '___'
  raise "Missing repo_url" if fetch(:repo_url).nil? or fetch(:repo_url) == 'git@bitbucket.org:___.git'
  raise "Missing branch" if fetch(:branch).nil?
  raise "Missing app_type" if fetch(:app_type).nil?
  raise "Incorrect app_type" if not ["gulp","next","nuxt","rails","wordpress","laravel"].include? fetch(:app_type)
end
