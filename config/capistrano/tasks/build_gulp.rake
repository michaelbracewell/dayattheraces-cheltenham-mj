desc "build gulp"
task :build_gulp do
  on roles(:app) do
    within release_path do 
      execute :npm, :install
      execute :bundle, :install

      with path: '/home/ubuntu/.rbenv/shims/:$PATH' do
        execute :gulp, "compile-min --staging --branded"
      end
    end
  end
end
