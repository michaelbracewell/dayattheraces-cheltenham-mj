desc "build next"
task :build_next do
  on roles(:app) do
    within release_path do 
      execute :bundle, :install
      execute :npm, :install

      with path: '/home/ubuntu/.rbenv/shims/:$PATH' do
        execute :npm, :run, :genS
      end
    end
  end
end
