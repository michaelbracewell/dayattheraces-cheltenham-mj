task :restart_puma do
  on roles(:all) do |host|
    execute "sudo systemctl restart puma-360.service"
  end
end