desc "update webserver config"
task :webserver_config_update do
  on roles(:app) do
    executable_path = "/opt/nginx-site-manager/current/app.rb"
    execute :ruby, executable_path, "--app-url", fetch(:application_url), "--app-path", fetch(:deploy_to), "--app-name", fetch(:application), "--app-type", fetch(:app_type)
  end
end