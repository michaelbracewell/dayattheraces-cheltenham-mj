desc "build nuxt"
task :build_nuxt do
  on roles(:app) do
    within release_path do 
      execute :npm, :install
      execute :npm, :run, :genS
    end
  end
end
