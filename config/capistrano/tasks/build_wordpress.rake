desc "build wordpress"
task :build_wordpress do
  on roles(:app) do
    within release_path do 
      within './gulp' do
        execute :npm, :install
        execute :gulp, 'compile-min', '--staging'
      end

      execute :composer, :install
      execute "sudo", "chown", "-R", "www-data:www-data", "#{fetch(:deploy_to)}/shared/public"
      execute "sudo", "service", "php7.2-fpm", "restart"
    end
  end
end
