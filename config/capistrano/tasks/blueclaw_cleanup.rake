desc "blueclaw cleanup"
task :blueclaw_cleanup do
  on roles(:app) do
    within release_path do 
      execute :find, '-maxdepth', '1', '-type', 'd', '-not', '-name', fetch(:publish_dir), '-not', '-name', '.', '-exec', 'rm', '-rf', '{} \;'
      execute :find, '.', '-maxdepth', '1', '-type', 'f', '-exec', 'rm', '-r', '{} \;'
      execute :mv, "#{fetch(:publish_dir)}/*", "."
      execute :find, fetch(:publish_dir), "-maxdepth", "1", "-type", "f", "-name", "'*'", "-exec", "mv", "-n", "{}", "./", '\;'
      execute :rm, "-rf", "#{fetch(:publish_dir)}"
      execute :echo, "Deployed #{fetch(:application)} project on #{fetch(:application_url)}"
    end
  end
end
