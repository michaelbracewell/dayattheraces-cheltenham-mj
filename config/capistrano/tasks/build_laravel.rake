desc "build laravel"
task :build_laravel do
  on roles(:app) do
    within release_path do 
      execute :composer, :install
      execute :npm, :install
      execute :npm, :run, :prod
      execute :php, :artisan, :migrate, "--force"

      within 'database' do
        execute :find, '.', '-maxdepth', '1', '-type', 'f', '-name', '*.sql', '-exec', 'rm', '{} \;'
      end

      execute :rm, "-rf", "node_modules"
      execute "sudo", "chown", "-R", "www-data:www-data", "#{fetch(:deploy_to)}/shared/storage"
      execute "sudo", "service", "php7.2-fpm", "restart"
    end
  end
end
