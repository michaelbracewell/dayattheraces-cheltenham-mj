import url from 'url';

// note: GA is now inputted directly do the app.html

export default {
  siteName: 'A Day at the Races | A beginners guide to Horse Racing', // human readable
  vanitySiteName: 'A Day at the Races | A beginners guide to Horse Racing', // human readable
  clientName: 'Mark Jarvis', // human readable
  get titleTemplate() {
    return `${this.clientName} | ${this.siteName}`;
  }, // template to be sent to vue meta. %s is string variable for current page.
  defaultDescription: 'New to Racing? Our Beginners guide to the biggest Horse Racing Events of the Year will guide you through the Sport of Kings',
  staging: {
    url: 'http://www.markjarvis-adayattheraces-chelthenham.staging.brewingatbc.co.uk', // ensure that the protocol is included here!
  },
  production: {
    domain: 'https://guide.mjsports.bet', // ensure that the protocol is included here!
    subfolder: '/at-the-races/cheltenham',
    get url() { return url.resolve(this.domain, this.subfolder); },
  },
  vanity: {
    url: '_VANITY_URL_',
  },
  canonical_url: 'https://guide.mjsports.bet/at-the-races/cheltenham', // Always fill this in. Always use production url
  twitter: {
    site: '',
    title: 'A Day at the Races | A beginners guide to Horse Racing',
    text: '',
    description: 'New to Racing? Our Beginners guide to the biggest Horse Racing Events of the Year will guide you through the Sport of Kings',
    creator: '',
    image: '',
  },
  facebook: {
    description: 'New to Racing? Our Beginners guide to the biggest Horse Racing Events of the Year will guide you through the Sport of Kings',
  },
  contentGoogleSheet: '1PYGxS6K9QFP5HorG7_8-8xiOD5HCRW_1RdNJqMSaXzY',

};
